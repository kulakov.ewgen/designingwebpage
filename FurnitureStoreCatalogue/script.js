
const furnitureData = [
    { id: 1, name: 'Boss chair', category: 'chair', price: 100, image: 'chair1.jpg', description: 'Comfortable chair for real bosses.' },
    { id: 2, name: 'Modern chair', category: 'chair', price: 60, image: 'chair2.jpg', description: 'Modern design chair for your beautiful home.' },
    { id: 3, name: 'Classic chair', category: 'chair', price: 40, image: 'chair3.jpg', description: 'Cool and comfortable classic chair.' },
    { id: 4, name: 'Dining table', category: 'table', price: 100, image: 'table1.jpg', description: 'Wooden dining table for family gatherings.' },
    { id: 5, name: 'Modern table', category: 'table', price: 120, image: 'table2.jpg', description: 'Glass top table with steel legs.' },
    { id: 6, name: 'L-shaped sofa', category: 'sofa', price: 300, image: 'sofa1.jpg', description: 'L-shaped sofa for your living room.' },
    { id: 7, name: 'Cozy sofa', category: 'sofa', price: 350, image: 'sofa2.jpg', description: 'Comfortable sofa with cushions.' }
];

function displayItems(items) {
    const itemsContainer = document.getElementById('itemsContainer');
    itemsContainer.innerHTML = '';

    items.forEach(item => {
        const itemElement = document.createElement('div');
        itemElement.classList.add('item');

        itemElement.innerHTML = `
            <img src="${item.image}" alt="${item.name}">
            <h3>${item.name}</h3>
            <p>${item.description}</p>
            <p>Price: $${item.price}</p>
            <button onclick="addItemToCart(${item.id})">Add to Cart</button>
        `;

        itemsContainer.appendChild(itemElement);
    });
}

function filterItems(category) {
    if (category === 'all') {
        displayItems(furnitureData);
    } else {
        const filteredItems = furnitureData.filter(item => item.category === category);
        displayItems(filteredItems);
    }
}

function searchItems() {
    const searchInput = document.getElementById('searchInput');
    const searchTerm = searchInput.value.toLowerCase();
    const filteredItems = furnitureData.filter(item => item.name.toLowerCase().includes(searchTerm));
    displayItems(filteredItems);
}

function addItemToCart(itemId) {
    const selectedItem = furnitureData.find(item => item.id === itemId);
    const cartItems = JSON.parse(localStorage.getItem('cartItems')) || [];
    cartItems.push(selectedItem);
    localStorage.setItem('cartItems', JSON.stringify(cartItems));
    displayCart();
}

function displayCart() {
    const cartItems = JSON.parse(localStorage.getItem('cartItems')) || [];
    const cartItemsList = document.getElementById('cartItems');
    cartItemsList.innerHTML = '';

    let totalPrice = 0;

    cartItems.forEach(item => {
        const cartItemElement = document.createElement('li');
        cartItemElement.textContent = `${item.name} - $${item.price}`;
        cartItemsList.appendChild(cartItemElement);

        totalPrice += item.price;
    });

    document.getElementById('totalPrice').textContent = `Total Price: $${totalPrice.toFixed(2)}`;
}

document.getElementById('categoryFilter').addEventListener('change', function() {
    filterItems(this.value);
});

document.getElementById('searchInput').addEventListener('input', searchItems);

document.getElementById('checkoutBtn').addEventListener('click', function() {
    alert('Checkout functionality will be implemented in the future!');
});

displayItems(furnitureData);
displayCart();
